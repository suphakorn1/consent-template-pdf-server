const express = require('express')
const puppeteer = require('puppeteer')
const app = express()
const cors = require('cors')

app.use(express.json())
app.use(cors())
app.use(express.static('public'))
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post('/pdf', async (req, res) => {
    const browser = await puppeteer.launch();
      const page = await browser.newPage()
      // We set the page content as the generated html by handlebars
      await page.setContent(req.body.content)
      // We use pdf function to generate the pdf in the same folder as this file.
      const pdfBuffer = await page.pdf({ path: 'public/invoice.pdf', format: 'A4' })
      await page.close();
      await browser.close();
      res.set("Content-Type", "application/pdf");
      console.log(__dirname + '/invoice.pdf')
    //   res.sendFile(__dirname + '/invoice.pdf');
      res.send(pdfBuffer);
})



app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})